# uxn tutorial

a beginner's guide for programming the varvara computer based on the {uxn} core, and a slow-paced companion to the official documentation.

=> https://wiki.xxiivv.com/site/uxn.html uxn technical documentation

the tutorial is divided in 8 days (or sections), as it can be followed along with a workshop.

(as of today, this is a work in progress)

there's a collaborative translation to spanish also in progress: {tutorial de uxn}

# day 1

in this first section of the tutorial we talk about the basics of the uxn computer called varvara, its programming paradigm in a language called uxntal, its architecture, and why you would want to learn to program it. 

we also jump right in into our first simple programs to demonstrate fundamental concepts that we will develop further in the following days. 

=> ./uxn_tutorial_day_1.gmi {uxn tutorial day 1}

# day 2

in this section we start exploring the visual aspects of the varvara computer: we talk about the fundamentals of the screen device so that we can start drawing on it! 

we also discuss working with shorts (2-bytes) besides single bytes in uxntal.

=> ./uxn_tutorial_day_2.gmi {uxn tutorial day 2}

=> ./img/screenshot_uxn-tiles.png screenshot of the output of the program, showing 16 squares colored with different combinations of outline and fill.

# day 3

here we introduce the use of the controller device in the varvara computer: this allows us to add interactivity to our programs, and to start implementing control flow in uxntal.

we also talk about logic and stack manipulation instructions in uxntal.

=> ./uxn_tutorial_day_3.gmi {uxn tutorial day 3}

=> ./img/screenshot_uxn-draw-with-keyboard.png screenshot of a possible result of running the following program; it shows a trail drawn with filled or outlined squares.

# day 4

here we discuss the animation loop of the varvara computer, via its screen device vector!

we also talk about using the program memory as a space for data via "variables", in order to have some persistency of data during the runtime of our programs, and/or in order to save us from complex stack wrangling :)

=> ./uxn_tutorial_day_4.gmi {uxn tutorial day 4}

# day 5

coming soon!

# instructions

this is a summary of the uxn instructions covered in each day of the tutorial.

## day 1

* ADD: take the top two elements from the stack, add them, and push down the result ( a b -- a+b )
* SUB: take the top two elements from the stack, subtract them, and push down the result ( a b -- a-b )
* LIT: push the next byte in memory down onto the stack
* DEO: output the given value into the given device address, both taken from the stack ( value address -- )

## day 2

* DEI: read a value into the stack, from the device address given in the stack ( address -- value )
* INC: increment the value at the top of the stack ( a -- a+1 )
* BRK: break the flow of the program, in order to close subroutines
* MUL: take the top two elements from the stack, multiply them, and push down the result ( a b -- a*b )
* DIV: take the top two elements from the stack, divide them, and push down the result ( a b -- a/b )
* SFT: take a shift value and a number to shift with that value, and shift it. the low nibble of the shift value indicates the shift to the right, and the high nibble the shift to the left ( number shift -- shiftednumber ) 

## day 3

* EQU: push 01 down into the stack if the top two elements of the stack are equal, 00 otherwise ( a b  -- a==b )
* NEQ: push 01 down into the stack if the top two elements of the stack are not equal, 00 otherwise ( a b  -- a!=b )
* GTH: push 01 down into the stack if the first element is greater than the second, 00 otherwise ( a b  -- a>b )
* LTH: push 01 down into the stack if the first element is less than the second, 00 otherwise ( a b  -- a>b )

* AND: perform a bitwise AND with the top two elements of the stack, and push down the result ( a b -- a&b )
* ORA: perform a bitwise OR with the top two elements of the stack, and push down the result ( a b -- a|b )
* EOR: perform a bitwise exclusive-OR with the top two elements of the stack, and push down the result ( a b -- a^b )

* JMP: unconditionally jump to the address in the stack ( addr -- )
* JCN: take an address and a value from the stack, and jump to the address if the value is not 00; otherwise continue with the next instruction ( value addr -- )

* POP: Remove top element from the stack ( a -- )
* DUP: Duplicate; push a copy of the top element ( a -- a a )
* SWP: Swap; change the order of the top two elements of the stack ( a b -- b a )
* NIP: Remove the top second element of the stack ( a b -- b )
* OVR: Over; push a copy of the second top element ( a b -- a b a )
* ROT: Rotate; reorder the top three elements of the stack so that the third one is now at the top ( a b c -- b c a )

## day 4

* LDA: load and push down into the stack the value at the given absolute address ( address -- value )
* STA: store into the given absolute address the value at the top of the stack ( value address -- )
* LDZ: load and push down into the stack the value at the given zero page address ( address -- value )
* STZ: store into the given zero page address the value at the top of the stack ( value address -- )
* LDR: load and push down into the stack the value at the given relative address ( address -- value )
* STR: store into the given relative address the value at the top of the stack ( value address -- )


# draft outline

this outline is here and now as a reference of the overall structure of the tutorial.

changes are expected to happen.

## day 1: the basics

* why uxn?
* {postfix} notation
* uxn computer architecture
* installation and toolchain
* a very basic hello world
* labels, macros, and runes
* an improved hello world
* print a digit

new instructions: LIT, DEO, ADD, SUB

=> https://git.sr.ht/~rabbits/uxn/ uxn repo

## day 2: the screen

* short mode
* system colors
* draw pixels
* sprites: chr format, nasu
* draw sprites
* stack operations
* practice: manual repetition of sprite

new instructions: DEI, MUL, DIV, SWP, OVR, ROT, DUP, POP

new mode: short mode

=> https://wiki.xxiivv.com/site/nasu.html nasu

## day 3: interactivity with the keyboard

* controller vector
* flow control: conditionals, relative and absolute jumps
* runes for addresses
* button and key
* bitwise masks
* practice: move/change sprite with keyboard

new instructions: EQU, NEQ, JCN, JMP, AND, ORA, EOR, SFT

## day 4: loops and animation

* flow control: repetition of a sprite
* screen vector
* variables: zero page, relative, absolute
* offsets in addresses
* animation timing
* practice: animated sprite

new instructions: LTH, GTH, STZ, STR, STA, LDZ, LDR, LDA

## day 5: interactivity with mouse

* mouse device and vector
* return stack and mode
* subroutines: parameters, calling, returning
* practice: sprite as pointer
* practice: simple drawing tool

new instructions: STH, JSR 

new mode: return mode

## day 6: audio

* the audio device
* samples as audio sprites
* adsr
* pitch
* practice: small music instrument

## day 7: keep mode and other devices

* keep mode
* re-writing code with keep mode
* file device: saving and loading a simple state
* datetime device: reading the date and time
* practice: visualization of time

new mode: keep mode

## day 8: demo time

* share what you created :)

# support

if you found this tutorial to be helpful, consider sharing it and giving it your {support} :)
