# coloring computers

non-electronic computers that work when you color them according to a simple set of rules.

an exploration of computation without electricity and semiconductors, an attempt to reinvent digital systems away from efficiency and productivity, and hopeful prototypes to expose the inner workings of computers. 

=> https://ipfs.io/ipfs/QmaiMEk5Stw5Xvfs1btAwMg2sctwEq1MS9NDJAUEr1SHvf/ coloring computers pack archive

related and inspired by some previous experiments like {arte generativo en papel}

# 4-bits to 7-segment display hexadecimal decoder (12020)

=> ./img/foto_20201130_hex7segdecoder_01.png the coloring computer/decoder, waiting to be activated
=> ./img/foto_20201130_hex7segdecoder_02.png a human coloring the wires according to the logic rules
=> ./img/foto_20201130_hex7segdecoder_03.png the coloring computer/decoder, with an input of 0011, and an output that can be read as 3

a coloring decoder built with NOT (triangle), AND (semicircle), and OR (the other shape (?)) gates ({compuertas}), based on a manual design. 

=> ./img/dibujo_20201207_hex7segdecoder_small.png the complete decoder

=> https://opguides.info/engineering/digitallogic/ colored and animated version by Vega
=> https://ipfs.io/ipfs/QmZv53hr7QEzxrPaRNpiyU9VUNHw9UgyaTUqYD9x9iFpNA/dibujo_20201207_hex7segdecoder.png download the decoder in full size 1487x3057 (png, ~446KB)

# computadora no(r)pal (12019)

=> ./img/dibujo_20190715-norpalera-fulladder_blanco_small.png logic circuit in the shape of nopal

a full-adder built with NOR gates (see {logiteca}) in the shape of no(r)pales

=> https://ipfs.io/ipfs/QmPz2D3bZRYFi1HnfiNJB8o9TZZvH8atuYpFixMKccYCYP/dibujo_20190715-norpalera-fulladder_blanco.png download computadora no(r)pal in full size 1200x1600 (png, ~429KB)

# coloring computers (12018)

the original ones

=> ./img/foto_coloring-computers_cover-lee.png photo of the cover of the zine, colored
=> ./img/foto_coloring-computers_7seg-lee.png photo of a pair of colored pages of the zine, with a 7 segment display showing the digits 2 and 3
=> ./img/foto_coloring-computers_pcd2019.png photo of a pair of colored pages of the zine, showing a digital circuit answering if two colors are the same

the booklet contains three series of computers: computers that compare, computers that count, and computers that play. they are all {nor}-based logic circuits designed by using truth tables, karnaugh maps, and maxterm expansions. 

=> https://ipfs.io/ipfs/QmYz7DPRWypGQcbAHr7Mi8EKB6ntSPsEnUsCXbAhBiHQZP/ original site and resources
=> https://ipfs.io/ipfs/QmYz7DPRWypGQcbAHr7Mi8EKB6ntSPsEnUsCXbAhBiHQZP/coloringcomputers_pages.pdf download the page-by-page zine (pdf, ~1.5MB)
=> https://ipfs.io/ipfs/QmYz7DPRWypGQcbAHr7Mi8EKB6ntSPsEnUsCXbAhBiHQZP/coloringcomputers.pdf download the ready-to-print-and-cut zine (pdf, ~1.4MB)

CC-BY-SA 4.0

for the print and cut zine: print double-sided, cut in half, fold the pages and assemble
