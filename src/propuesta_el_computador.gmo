# El Computador: Coreografía estructurada como microprocesador (Propuesta)

El siguiente texto fue parte de mi aplicación a la convocatoria de jóvenes creadores (1)2015 en la disciplina de danza. 

Probablemente es el primer precedente escrito del proyecto de compudanzas.

La propuesta no fue aceptada en la fase administrativa.

# Características del proyecto

## Sinopsis

El microprocesador es una máquina diseñada para repetidamente leer y ejecutar instrucciones sencillas. La complejidad de las operaciones computacionales, que van desde calcular una raíz cuadrada hasta simular el inicio del Universo, surge a partir de la combinación adecuada de pasos a seguir. El Computador es coreografía estructurada como microprocesador: las repeticiones de un cuerpo llevado a sus límites físicos lo trascienden porque está ejecutando un algoritmo que busca una respuesta.

## Introducción

En la búsqueda de diferentes propuestas estructurales en la coreografía, resulta interesante someter al cuerpo humano a planteamientos rígidos y basados en repeticiones, como podemos notar en obras de creadores minimalistas. Este proyecto parte de esas ideas, y se pregunta: ¿Qué pasaría si las repeticiones de acciones físicas, además de tener un sentido estético y estructural, fueran acumulando sus efectos en un plano abstracto para llegar a un resultado tangible?

Es posible partir de una disciplina que entre otros objetos estudia las secuencias, las repeticiones, y los resultados de sus combinaciones: las ciencias computacionales. Una computadora es una máquina basada en la repetición, y esta se usa de tal forma que sus acciones se acumulan de acuerdo al diseño dado por un algoritmo para logran llegar a soluciones.

¿Qué pasa entonces si un cuerpo humano se convierte en una computadora, en donde sus acciones repetidas en una coreografía son en realidad parte de un programa que tiene un objetivo? En primera instancia este hecho puede verse como una representación de las rutinas en la sociedad actual, y los efectos de la repetición estricta sobre una persona, pero en las realidad hay más implicaciones.

Por un lado, las ciencias computacionales, como las matemáticas, pueden ser sumamente abstractas, manejando conceptos elevados donde además se busca la elegancia y la simplicidad para expresar gran complejidad. Se llega a decir que "el código es poesía", por lo que esta coreografía sería una representación de ese punto de vista estético. Por otro lado, las computadoras en la realidad trabajan a velocidades que es difícil imaginar: cuando se dice que un microprocesador trabaja a 2 GHz, por ejemplo, significa que está realizando 2,000'000,000 operaciones durante un segundo. Un humano replicando su funcionamiento estaría alentando el tiempo en una proporción inmensa, en la que por ejemplo unos cuantos años se transformarían en el periodo correspondiente a la edad del Universo. Finalmente, actualmente vivimos rodeados de dispositivos digitales, pero es poco conocido cómo funcionan en realidad, o cómo son una maravilla del ingenio humano.

Para tener una idea más clara de las implicaciones del proyecto coreográfico, previo a describirlo con mayor profundidad se presenta a continuación un breve marco teórico sobre algoritmos y arquitectura computacional.

## Breve marco teórico sobre pensamiento algorítmico

Un algoritmo es una secuencia finita y ordenada de pasos o instrucciones que buscan resolver un problema. Por ejemplo, cualquier receta de cocina es un algoritmo, pues combina elementos en una secuencia y tiene el fin de preparar un platillo. En las ciencias computacionales y en las matemáticas, los algoritmos son estudiados por su capacidad de resolver problemas diversos, como cálculos numéricos, control de sistemas automatizados, telecomunicaciones, administración de datos, por citar algunos.

Las instrucciones de un algoritmo computacional pueden clasificarse en cuatro grandes grupos:

* Instrucciones de entrada de datos, para recibir los materiales a procesar en el algoritmo.
* Instrucciones de salida de datos, para entregar los resultados del algoritmo.
* Instrucciones aritméticas, lógicas, y de otros procesamientos, que realizan operaciones sobre los datos.
* Instrucciones de control, que permiten tomar decisiones condicionales ("Si sucede X, haz Y, si no haz Z") y construir estructuras de repetición ("Haz X 3 veces" o "Haz Y mientras Z no suceda")

Cualquier problema que se ha resuelto o se puede resolver con un algoritmo, tiene una solución construida únicamente con una combinación adecuada de esos cuatro elementos. Esto implica que cualquier máquina capaz de ejecutar este tipo de instrucciones, al seguir el algoritmo correcto teóricamente puede resolver cualquiera de esa clase de problemas.

## Breve marco teórico sobre arquitectura computacional

Alan Turing propuso en 1936 el modelo de lo que después se llamaría Máquina de Turing: un dispositivo hipotético que se desliza sobre una cinta con símbolos que son leídos y manipulados de acuerdo a una tabla de reglas, y que al hacerlo puede ejecutar un algoritmo computacional. En la misma publicación estableció lo que se llamaría Máquina Universal de Turing: un dispositivo hipotético de la misma naturaleza pero que puede simular a cualquier otra máquina de Turing de acuerdo a los símbolos escritos sobre la cinta. Estas ideas y su estudio, además de ser pilares para las ciencias computacionales, dieron paso a lo que se conoce actualmente como arquitectura de von Neumann.

La arquitectura de von Neumann consiste en la descripción funcional de un sistema electrónico digital que funge como computadora: un dispositivo con la capacidad de ejecutar algoritmos codificados en una serie de instrucciones denominadas programa. Consta de cuatro componentes básicos: Unidad de Procesamiento Central (CPU o microprocesador), Memoria, Dispositivos de Entrada, y Dispositivos de Salida. La Unidad de Procesamiento Central debe tener a su vez una Unidad de Control y una Unidad Aritmética y Lógica (ALU).


Lo más relevante de la arquitectura de von Neumann para este proyecto se enlista a continuación:

* La Unidad Central de Procesamiento está diseñada para ser capaz de ejecutar un conjunto pequeño de instrucciones sencillas. Además, su función es ejecutar una instrucción a la vez.
* La Memoria almacena de manera ordenada tanto las instrucciones del algoritmo, también llamadas programa, como los datos de apoyo.
* La Unidad de Control guía la secuencia de acciones, estableciendo cuándo y dónde leer una instrucción de la memoria, y qué hacer dependiendo de la lectura.
* El funcionamiento de la computadora, llamado Ciclo de Instrucción (Machine instruction cycle) se divide en dos grandes etapas que se repiten sin fin a una gran velocidad: etapa de lectura (fetch cycle), en el que la Unidad Central de Procesamiento busca en la Memoria la siguiente instrucción a ejecutar, y etapa de ejecución (execute cycle) en el que la decodifica, la ejecuta, y se calcula la ubicación de la siguiente instrucción.

Debido a lo complejo que puede resultar comprender y utilizar el funcionamiento de estas máquinas, se han desarrollado modelos como el de Little Man Computer (Computadora del hombre pequeño) de Stuart Madnick. En esta representación, la Unidad de Control se piensa como un hombre que se encuentra en una habitación y sigue una secuencia específica de pasos para leer y ejecutar instrucciones y datos guardados en una serie de buzones ordenados. El hombre, al igual que los microprocesadores, solo obedecen las instrucciones en el orden en el que se le dan, una a la vez. Como sea, al seguir instrucciones correspondientes a diferentes algoritmos, pueden resolver tareas de gran complejidad sin tener que ser conscientes del problema que están resolviendo.

## La coreografía de El Computador

Con base en el modelo de la Little Man Computer, la propuesta coreográfica de este proyecto consiste en construir con las acciones físicas de una persona sujetas a una serie de reglas, una computadora funcional y programable. Después de establecer de manera abstracta las características del microprocesador y sus operaciones, y de la arquitectura de la computadora completa, es posible determinar cómo las etapas del fetch cycle y del execute cycle serán implementadas con acciones del intérprete. Probablemente debe utilizarse utilería auxiliar para la representación de los números en la computadora, que son la base para codificar las instrucciones del microprocesador, las ubicaciones en la Memoria, y los datos con los que hacer operaciones. Ejemplos posibles son rocas, pizarrones, pelotas, globos, etc.

Ya con la computadora construida de esta forma, se tendrán infinitas coreografías en potencia: el algoritmo que se programe en la Memoria, determinará la secuencia de las repeticiones de lectura y ejecución (fetch y execute). Así, lo que podremos ver en escena es a una persona trasladándose repetidamente y realizando operaciones con su cuerpo y la utilería. Como sea, el acto escénico, además de ser una estructura coreográfica basada en repetición, estará realizando alguna operación preestablecida por su programa.

## Breve ejemplo de programa

Una operación relativamente sencilla a resolver para ejemplificar el proceso y un esbozo de la coreografía resultante, es la suma de dos números recibidos por la computadora, y la entrega de sus resultados. El algoritmo, numerado por instrucciones, sería algo como:

* 1) Pedir número y guardarlo en el registro temporal
* 2) Copiar el número del registro temporal a una posición X de la Memoria
* 3) Pedir número y guardarlo el registro temporal
* 4) Sumar el registro temporal con el número en la posición X de la Memoria
* 5) Entregar el resultado presente en el registro temporal

Al momento de su ejecución coreográfica, siguiendo el Ciclo de Instrucción, la secuencia de acciones quedaría algo como:

* Fetch: Intérprete se dirige a la posición #1 en la Memoria y se lleva el número de la instrucción ahí guardada
* Execute: Intérprete regresa y ve qué acciones debe tomar de acuerdo al número de la instrucción. Como la instrucción dice: Pedir número y guardarlo en el registro temporal, el intérprete se dirige a la zona de entrada de datos a esperar un número. Ya que se recibe el número, el intérprete lo lleva al registro temporal. Como la instrucción terminó, se dirige a ver cuál es el número de la siguiente instrucción (#2).
* Fetch: Intérprete se dirige a la posición #2 en la Memoria y se lleva el número de la instrucción ahí guardada
* Execute: Intérprete regresa y ve qué acciones debe tomar de acuerdo al número de la instrucción. Como la instrucción dice: Copiar el número del registro temporal a una posición X de la Memoria, el intérprete se dirige al registro temporal, copia el número ahí presente, y lo lleva a la posición X en la Memoria. Como la instrucción terminó, se dirige a ver cuál es el número de la siguiente instrucción (#3).
* Fetch: Intérprete se dirige a la posición #3 en la Memoria y se lleva el número de la instrucción ahí guardada
* Execute: Intérprete regresa y ve qué acciones debe tomar de acuerdo al número de la instrucción. Como la instrucción dice: Pedir número y guardarlo en el registro temporal, el intérprete se dirige a la zona de entrada de datos a esperar un número. Ya que se recibe el número, el intérprete lo lleva al registro temporal. Como la instrucción terminó, se dirige a ver cuál es el número de la siguiente instrucción (#4).
* Fetch: Intérprete se dirige a la posición #4 en la Memoria y se lleva el número de la instrucción ahí guardada
* Execute: Intérprete regresa y ve qué acciones debe tomar de acuerdo al número de la instrucción. Como la instrucción dice: Sumar el registro temporal con el número en la posición X de la Memoria, se dirige a la posición X de la Memoria, copia el número ahí presente y lo trae de regreso, se dirige al registro temporal y toma el número ahí presente, pasa ambos números por un proceso sumador, y el resultado lo coloca en el registro temporal. Como la instrucción terminó, se dirige a ver cuál es el número de la siguiente instrucción (#5).
* Fetch: Intérprete se dirige a la posición #5 en la Memoria y se lleva el número de la instrucción ahí guardada
* Execute: Intérprete regresa y ve qué acciones debe tomar de acuerdo al número de la instrucción. Como la instrucción dice: Entregar el resultado presente en el registro temporal, el intérprete se dirige al registro temporal, copia el número ahí presente, y lo lleva a la zona de salida de datos. Como la instrucción terminó, se dirige a ver cuál es el número de la siguiente instrucción (#6).
* Fetch: Intérprete se dirige a la posición #6 en la Memoria y se lleva el número de la instrucción ahí guardada
* Execute: Intérprete regresa y ve qué acciones debe tomar de acuerdo al número de la instrucción. Como ya no hay instrucción, el programa se termina.


# Descripción de las actividades

Las actividades las podemos dividir en cuatro grandes etapas: El diseño abstracto de la arquitectura computacional, la realización de la computadora, la programación de la computadora, y la realización de objetos coreográficos y didácticos.

## Diseño abstracto de la arquitectura computacional

Esta etapa tiene como objetivo la creación del modelo computacional sobre el cual se realizará físicamente las operaciones. Básicamente hay tres componentes a establecer:

* El conjunto de instrucciones del microprocesador con sus códigos numéricos y descripciones.
* La microprogramación del microprocesador: cómo realizar el fetch cycle y el execute cycle para cada instrucción, paso a paso.
* La arquitectura computacional: cómo se conectarán y comunicarán los componentes, cómo se representarán los datos e instrucciones de forma numérica, incluyendo el rango de los números a utilizar, y cuál será el tamaño de la Memoria

## Realización de la computadora

En esta etapa el modelo computacional se debe aterrizar al mundo físico para que pueda funcionar. Los tres componentes básicos a establecer son:

* La implementación escenográfica de los componentes de la computadora: cómo se representarán los números en escena, tomando en cuenta su tamaño y las posibilidades de copiarlos y hacer operaciones con ellos; cómo se representarán las casillas ordenadas de la Memoria, y cómo se representarán las áreas funcionales de la computadora.
* Las acciones físicas para hacer funcionar a la computadora, interactuando con los datos numéricos y haciendo operaciones sobre ellos.
* La velocidad de operación de la computadora, procurando un entrenamiento cardiovascular completo para que el cuerpo se pueda encontrar bajo una gran demanda durante la ejecución de los programas.

## Programación

En esta etapa, con la computadora ya construida, es posible escribir y probar diversos programas, buscando:

* Que usen datos de entrada para pedirlos en escena y el algoritmo funcione a partir de ellos
* Complejidad y estética algorítmica ("la poesía del código")
* Ejecuciones físicas estéticamente interesantes
* Diferentes duraciones: 5, 10, 20, y 60 minutos.

## Objetos coreográficos y didácticos

El objetivo de esta etapa es desarrollar materiales multimedia para describir o replicar el funcionamiento de la computadora a partir del hecho coreográfico. Además, el proyecto puede tener una carga fuertemente didáctica sobre las ciencias computacionales, por lo que será necesario establecer estrategias para divulgar el conocimiento a partir del material creado.

# Objetivos que se pretenden conseguir

* Desarrollar de una obra coreográfica que funcione como una computadora programable de acuerdo a las descripciones anteriores.
* Desarrollar programas de diferentes duraciones para la computadora, que usen datos de entrada a pedir en escena y funcionen a partir de ellos.
* Construir objetos coreográficos y didácticos para utilizar a la obra como material de divulgación científica y artística.

# Referencias

=> http://www.alanturing.net/turing_archive/pages/Reference%20Articles/BriefHistofComp.html A Brief History of Computing. 
=> http://www.yorku.ca/sychen/research/LMC/ Little Man Computer.
=> https://xkcd.com/505 Munroe, R. A bunch of rocks.

* Turing, A.M. (1936). "On Computable Numbers, with an Application to the Entscheidungs problem". Proceedings of the London Mathematical Society. 2 (1937) 42: 230–265.
* von Neumann, John (1945), First Draft of a Report on the EDVAC
